  $(function () {
        $("[data-toggle=tooltip]").tooltip();
        $("[data-toggle=popover]").popover();
        $('.carousel').carousel({
            interval: 10000
        });
        $('#contacto').on('show.bs.modal', function (e) {

            console.log('el modal se está mostrando');
            $('#contactoBtn1').removeClass('btn-outline-success');
            $('#contactoBtn2').removeClass('btn-outline-success');
            $('#contactoBtn3').removeClass('btn-outline-success');
            $('#contactoBtn1').addClass('btn-danger');
            $('#contactoBtn2').addClass('btn-danger');
            $('#contactoBtn3').addClass('btn-danger');
            $('#contactoBtn1').prop('disabled', true);
            $('#contactoBtn2').prop('disabled', true);
            $('#contactoBtn3').prop('disabled', true);

        });
        $('#contacto').on('shown.bs.modal', function (e) {
            console.log('el modal se mostró');
        });
        $('#contacto').on('hide.bs.modal', function (e) {
            console.log('el modal contacto se oculta');
        });

        $('#contacto').on('hidden.bs.modal', function (e) {
            console.log('el modal  se ocultó');
            $('#contactoBtn1').prop('disabled', false);
            $('#contactoBtn1').removeClass('btn-danger');
            $('#contactoBtn1').addClass('btn-outline-success');
            $('#contactoBtn2').prop('disabled', false);
            $('#contactoBtn2').removeClass('btn-danger');
            $('#contactoBtn2').addClass('btn-outline-success');
            $('#contactoBtn3').prop('disabled', false);
            $('#contactoBtn3').removeClass('btn-danger');
            $('#contactoBtn3').addClass('btn-outline-success');
        });
    });